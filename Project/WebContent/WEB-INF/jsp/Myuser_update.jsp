<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>ユーザ情報更新</title>
<link href="user.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">


</head>

<body>
	<div class="alert alert-dark" role="alert">

		<ul class="nav justify-content-end">
			<li class="nav-item"><a class="nav-link active">${userInfo.name}さん</a></li>


			<li class="nav-item"><a class="nav-link active"
				href="MyLoginServlet"><span style="color: red">ログアウト</span></a></li>
		</ul>
	</div>

	<div style="text-align: center;">
		<h1>ユーザ情報更新</h1>
		<c:if test="${errMsg != null}"  style="color: red;">
	${errMsg}
	</c:if>
		<br>






		<div class="form-group row">
			<label for="inputID" class="col-sm-2 col-form-label">ログインID</label>
			<div class="col-sm-1">
				<p>${user.loginId }</p>
			</div>
		</div>
		<form action="MyUserUpdateServlet" method="post">
			<input type="hidden" name="id" value="${user.id}">

			<div class="form-group row">
				<label for="inputID" class="col-sm-2 col-form-label">パスワード</label>
				<div class="col-sm-6">
					<input type="password" class="form-control" id="inputPassword"
						name="password" placeholder="password">
				</div>
			</div>
			<div class="form-group row">
				<label for="inputID" class="col-sm-2 col-form-label">パスワード(確認)</label>
				<div class="col-sm-6">
					<input type="password" class="form-control" id="inputPassword"
						name="password2" placeholder="password">
				</div>
			</div>
			<div class="form-group row">
				<label for="inputID" class="col-sm-2 col-form-label">ユーザ名</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" id="inputPassword"
						name="name" placeholder="User name" value="${user.name}">
				</div>
			</div>



			<div class="form-group row">
				<label for="inputID" class="col-sm-2 col-form-label">生年月日</label>
				<div class="col-sm-1">
					<input type="date" max="9999-12-31" name="birthDate"value="${user.birthDate}">
				</div>
			</div>






		<br> <br> <input type="submit" value="更新" >


		</form>
	</div>
	<a href="MyUserListServlet">戻る</a>
</body>

</html>