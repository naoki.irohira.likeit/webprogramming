<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
<link href="user.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">



</head>

<body>
<div style="text-align: center;">
	<h1 >ログイン画面</h1>

	<c:if test="${errMsg != null}"  style="color: red;">
	${errMsg}
	</c:if>
	<br>

		<form class="form-signin" action="MyLoginServlet" method="post">

			<div class="form-group row">
				<label for="inputID" class="col-sm-2 col-form-label">ログインID</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" id="inputPassword"
						placeholder="ID" name="loginId">
				</div>
			</div>
			<div class="form-group row">
				<label for="inputID" class="col-sm-2 col-form-label">パスワード</label>
				<div class="col-sm-6">
					<input type="password" class="form-control" id="inputPassword"
						placeholder="password" name="password">
				</div>
			</div>



			<input type="submit" value="ログイン">
		</form>
	</div>

</body>

</html>