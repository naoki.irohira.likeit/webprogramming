<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>ユーザ一覧</title>
<link href="user.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">


</head>


<body>

	<div class="alert alert-dark" role="alert">

		<ul class="nav justify-content-end">
			<li class="nav-item"><a class="nav-link active">${userInfo.name}さん</a></li>


			<li class="nav-item"><a class="nav-link active"
				href="MyLoginServlet"><span style="color: red">ログアウト</span></a></li>
		</ul>
	</div>


	<h1 style="text-align: center">ユーザ一覧</h1>
	<br>
	<a href="MyUserEntryServlet" class="newEntry">新規登録</a>
	<div style="text-align: center;">

		<form action="MyUserListServlet" method="post">

			<div class="form-group row">
				<label for="inputID" class="col-sm-2 col-form-label">ログインID</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" id="inputPassword"
						name="loginId" placeholder="ID">
				</div>
			</div>
			<div class="form-group row">
				<label for="inputID" class="col-sm-2 col-form-label">ユーザ名</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" id="inputPassword"
						name="name" placeholder="User name">
				</div>
			</div>
			<div class="form-group row">
				<label for="inputID" class="col-sm-2 col-form-label">生年月日</label>
				<div class="col-sm-1">
					<input type="date" name="birthDate" max="9999-12-31"
						>


					<div class="form-group row">
						<label for="inputID" class="col-sm-2 col-form-label">～</label>
						<div class="col-sm-1">
							<input type="date" name="birthDate2" max="9999-12-31"
								>
						</div>
					</div>


				</div>
			</div>

			<br> <br> <input type="submit" value="検索">
		</form>
	</div>
	<div class="table-responsive">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>ログインID</th>
					<th>ユーザ名</th>
					<th>生年月日</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="MyUser" items="${userList}">
					<tr>

						<td>${MyUser.loginId}</td>
						<td>${MyUser.name}</td>
						<td>${MyUser.birthDate}</td>

						<!-- TODO 未実装；ログインボタンの表示制御を行う -->
						<td><a class="btn btn-primary"
							href="MyUserDetailServlet?id=${MyUser.id}">詳細</a> <c:if
								test="${userInfo.loginId== 'admin'|| userInfo.loginId== MyUser.loginId}">
								<a class="btn btn-success"
									href="MyUserUpdateServlet?id=${MyUser.id}">更新</a>
							</c:if> <c:if test="${userInfo.loginId== 'admin'}">
								<a class="btn btn-danger"
									href="MyUserDeleteServlet?id=${MyUser.id}">削除</a>
							</c:if></td>






					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>


</body>

</html>