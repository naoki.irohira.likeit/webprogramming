<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>ユーザ情報詳細参照</title>
<link href="user.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">


</head>

<body>
<body>
	<div class="alert alert-dark" role="alert">

		<ul class="nav justify-content-end">
			<li class="nav-item"><a class="nav-link active">${userInfo.name}さん</a></li>


			<li class="nav-item"><a class="nav-link active"
				href="MyLoginServlet"><span style="color: red">ログアウト</span></a></li>
		</ul>
	</div>


	<div style="text-align: center;">




		<h1>ユーザ情報詳細参照</h1>
		<br>







		<p1>ログインID</p1>
		<p1>${user.loginId }</p1>
		<br> <br>



		<p2>ユーザ名</p2>
		<p2>${user.name }</p2>

		<br> <br>


		<p3>生年月日</p3>
		<p3>${user.birthDate}</p3>

		<br> <br>


		<p4>登録日時 </p4>
		<p4>${user.createDate} </p4>

		<br> <br>


		<p5>更新日時</p5>
		<p5>${user.updateDate }</p5>

		<br> <br>
	</div>

	<a href="MyUserListServlet">戻る</a>
</body>

</html>