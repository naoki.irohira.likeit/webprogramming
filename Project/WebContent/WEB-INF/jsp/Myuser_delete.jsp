<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>ユーザ削除確認</title>
<link href="user.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">


</head>

<body>
<body>
	<div class="alert alert-dark" role="alert">

		<ul class="nav justify-content-end">
			<li class="nav-item"><a class="nav-link active">${userInfo.name}さん</a></li>


			<li class="nav-item"><a class="nav-link active"
				href="MyLoginServlet"><span style="color: red">ログアウト</span></a></li>
		</ul>
	</div>

	<div style="text-align: center;">
		<h1>ユーザ削除確認</h1>
		<br>
		<h4>ログインID:${user.loginId}</h4>
		<h4>を本当に削除してよろしいでしょうか。</h4>

		<br> <br> <input type="submit"
			onclick="location.href='MyUserListServlet'" value="キャンセル">
			<form action="MyUserDeleteServlet" method="post" >
			<input type="hidden" name="id" value="${user.id}">
			<input
			type="submit" value="OK">
			</form>

	</div>

</body>

</html>