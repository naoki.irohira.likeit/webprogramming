<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>ユーザ新規登録</title>
<link href="user.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">


</head>

<body>
	<div class="alert alert-dark" role="alert">

		<ul class="nav justify-content-end">
			<li class="nav-item"><a class="nav-link active">${userInfo.name}さん</a></li>


			<li class="nav-item"><a class="nav-link active"
				href="MyLoginServlet"><span style="color: red">ログアウト</span></a></li>
		</ul>
	</div>

	<div style="text-align: center;">
		<h1>ユーザ新規登録</h1>
		<c:if test="${errMsg != null}"  style="color: red;">
	${errMsg}
	</c:if>
		<br>


		<form action="MyUserEntryServlet" method="post">

			<div class="form-group row">
				<label for="inputID" class="col-sm-2 col-form-label">ログインID</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" id="inputPassword"
						name="loginId" placeholder="ID">
				</div>
			</div>
			<div class="form-group row">
				<label for="inputID" class="col-sm-2 col-form-label">パスワード</label>
				<div class="col-sm-6">
					<input type="password" class="form-control" id="inputPassword"
						name="password" placeholder="password">
				</div>
			</div>
			<div class="form-group row">
				<label for="inputID" class="col-sm-2 col-form-label">パスワード(確認)</label>
				<div class="col-sm-6">
					<input type="password" class="form-control" id="inputPassword"
						name="password2" placeholder="password">
				</div>
			</div>
			<div class="form-group row">
				<label for="inputID" class="col-sm-2 col-form-label">ユーザ名</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" id="inputPassword"
						name="name" placeholder="User name">
				</div>
			</div>



			<p>生年月日 </p>
			<input type="date" max="9999-12-31" name="birthDate">
			<br> <br> <input type="submit" value="登録" href="MyUserListServlet">
		</form>
	</div>
	<a href="MyUserListServlet">戻る</a>



</body>

</html>