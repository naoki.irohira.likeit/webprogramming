package myController;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import myDao.MyUserDao;
import myModel.MyUser;

/**
 * Servlet implementation class MyUserEntry
 */
@WebServlet("/MyUserEntryServlet")
public class MyUserEntryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MyUserEntryServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Myuser_entry.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String createDate = request.getParameter("create_date");
		String updateDate = request.getParameter("update_date");

		MyUserDao userDao = new MyUserDao();
		MyUser user = userDao.error(loginId);

		if (!(password.equals(password2)) || user != null || password.equals("") || name.equals("")
				|| password2.equals("")
				|| birthDate.equals("") || loginId.equals("")) {

			request.setAttribute("errMsg", "入力された内容は正しくありません");

			// ENTRYjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Myuser_entry.jsp");
			dispatcher.forward(request, response);
			return;

		}
		String result = MyUserDao.password(password);
		MyUser user1 = userDao.entry(loginId, name, birthDate, result, password2, createDate, updateDate);
		response.sendRedirect("MyUserListServlet");
	}
}
