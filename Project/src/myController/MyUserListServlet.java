package myController;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import myDao.MyUserDao;
import myModel.MyUser;

/**
 * Servlet implementation class MyUserListServlet
 */
@WebServlet("/MyUserListServlet")
public class MyUserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MyUserListServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO 未実装：ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
		MyUser user = (MyUser) session.getAttribute("userInfo");
		if (user == null) {
			response.sendRedirect("MyLoginServlet");
			return;
		}
		// ユーザ一覧情報を取得
		MyUserDao MyUserDao = new MyUserDao();
		List<MyUser> MyUserList = MyUserDao.findAll();

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("userList", MyUserList);

		// ユーザ一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Myuser_list.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO  未実装：検索処理全般
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		String birthDate2 = request.getParameter("birthDate2");

		MyUserDao userDao = new MyUserDao();
		List<MyUser> user = userDao.search(loginId, name, birthDate, birthDate2);
		request.setAttribute("userList", user);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Myuser_list.jsp");
		dispatcher.forward(request, response);

	}

}
