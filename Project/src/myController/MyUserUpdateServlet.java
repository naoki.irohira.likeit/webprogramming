package myController;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import myDao.MyUserDao;
import myModel.MyUser;

/**
 * Servlet implementation class MyUserUpdateServlet
 */
@WebServlet("/MyUserUpdateServlet")
public class MyUserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MyUserUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		// TODO  未実装：idを引数にして、idに紐づくユーザ情報を出力する
		MyUser user = MyUserDao.detail(id);
		// TODO  未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード
		request.setAttribute("user", user);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Myuser_update.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");

		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");

		MyUserDao userDao = new MyUserDao();

		if (password.equals("") && password2.equals("")) {

			MyUser user = userDao.update2(name, birthDate, id);
			response.sendRedirect("MyUserListServlet");
			return;
		}

		if (!(password.equals(password2)) || name.equals("") ||
				birthDate.equals("")) {

			request.setAttribute("errMsg", "入力された内容は正しくありません");

			// ENTRYjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Myuser_update.jsp");
			dispatcher.forward(request, response);
			return;

		}
		String result = MyUserDao.password(password);
		MyUser user = userDao.update(id, result, password2, name, birthDate);
		response.sendRedirect("MyUserListServlet");

	}

}
