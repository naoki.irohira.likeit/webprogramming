//beans
package myModel;

import java.io.Serializable;
import java.sql.Date;

public class MyUser implements Serializable {
	private int id;
	private String loginId;
	private String name;
	private Date birthDate;
	private String password;
	private String createDate;
	private String updateDate;
	private Date birthDate2;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public MyUser(int id, String loginId, String name, Date birthDate, String password, String createDate,
			String updateDate) {
		super();
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.password = password;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	public MyUser(int id1, String name1, String loginId1, Date birthDate1) {
		this.id = id1;
		this.name = name1;
		this.loginId = loginId1;
		this.birthDate = birthDate1;


	}

	public MyUser(String loginIdData, String nameData) {
		this.loginId = loginIdData;
		this.name = nameData;
	}

	public MyUser(String loginId, String name, Date birthDate, String createDate,
			String updateDate) {

		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.createDate = createDate;
		this.updateDate = updateDate;

	}

	public MyUser(String loginId, String name, Date birthDate, String createDate, String updateDate, int id1) {
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.createDate = createDate;
		this.updateDate = updateDate;
		this.id = id1;

	}

	public MyUser(String name1) {

		this.name = name1;
	}
}
