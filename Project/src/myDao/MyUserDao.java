package myDao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import myModel.MyUser;

public class MyUserDao {
	/**
	* ログインIDとパスワードに紐づくユーザ情報を返す
	* @param loginId
	* @param password
	* @return
	**/
	public MyUser findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = MyDBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new MyUser(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * 全てのユーザ情報を取得する
	 * @return
	 */
	public List<MyUser> findAll() {
		Connection conn = null;
		List<MyUser> userList = new ArrayList<MyUser>();

		try {
			// データベースへ接続
			conn = MyDBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user where not login_id='admin'";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				MyUser user = new MyUser(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public MyUser entry(String loginId, String name, String birthDate, String password, String password2,
			String createDate,
			String updateDate) {

		/**
		 * Servlet implementation class MyUserEntry
		 */

		Connection conn = null;
		try {

			// データベースへ接続
			conn = MyDBManager.getConnection();

			// SELECT文を準備
			String sql = "INSERT INTO user(login_id,name,birth_date,password,create_date,update_date)VALUES(?,?,?,?,now(),now())";

			// SELECTを実行し、結果表（ResultSet）を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, name);
			pStmt.setString(3, birthDate);
			pStmt.setString(4, password);

			pStmt.executeUpdate();

			pStmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return null;

	}

	public static MyUser detail(String id) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = MyDBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);

			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			int id1 = rs.getInt("id");

			return new MyUser(loginId, name, birthDate, createDate, updateDate, id1);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}

	public static MyUser update(String id, String password, String password2, String name, String birthDate) {

		/**
		 * Servlet implementation class MyUserEntry
		 */

		Connection conn = null;
		try {

			// データベースへ接続
			conn = MyDBManager.getConnection();

			// SELECT文を準備
			String sql = "UPDATE user set password=?,name=?,birth_date=? ,update_date=now() where id=? ";

			// SELECTを実行し、結果表（ResultSet）を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, password);
			pStmt.setString(2, name);
			pStmt.setString(3, birthDate);
			pStmt.setString(4, id);

			pStmt.executeUpdate();

			pStmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return null;

		// TODO 自動生成されたメソッド・スタブ

	}

	public MyUser update2(String name, String birthDate, String id) {

		Connection conn = null;
		try {

			// データベースへ接続
			conn = MyDBManager.getConnection();

			// SELECT文を準備
			String sql = "UPDATE user set name=? ,birth_date=?, update_date=now() where id=? ";

			// SELECTを実行し、結果表（ResultSet）を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, name);
			pStmt.setString(2, birthDate);
			pStmt.setString(3, id);

			pStmt.executeUpdate();

			pStmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		// TODO 自動生成されたメソッド・スタブ
		return null;
	}

	public MyUser delete(String id) {

		Connection conn = null;
		try {

			// データベースへ接続
			conn = MyDBManager.getConnection();

			// SELECT文を準備
			String sql = "DELETE FROM user WHERE id=?";

			// SELECTを実行し、結果表（ResultSet）を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, id);

			pStmt.executeUpdate();

			pStmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return null;
	}

	public List<MyUser> search(String loginId, String name, String birthDate, String birthDate2) {
		Connection conn = null;
		List<MyUser> userList = new ArrayList<MyUser>();

		try {
			// データベースへ接続
			conn = MyDBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE not login_id='admin'";

			if (!(name.equals(""))) {
				sql += " and name like '%" + name + "%' ";
			}

			if (!(loginId.equals(""))) {
				sql += " and login_id = '" + loginId + "' ";
			}
			if (!(birthDate.equals(""))) {
				sql += " and birth_date >= '" + birthDate + "'";
			}

			if (!(birthDate2.equals(""))) {
				sql += " and birth_date <= '" + birthDate2 + "'";
			}

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {

				String name1 = rs.getString("name");
				int id1 = rs.getInt("id");
				String loginId1 = rs.getString("login_id");
				Date birthDate1 = rs.getDate("birth_date");

				MyUser user = new MyUser(id1, name1, loginId1, birthDate1);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public static MyUser error(String loginId) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = MyDBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);

			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginId1 = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			int id1 = rs.getInt("id");

			return new MyUser(loginId1, name, birthDate, createDate, updateDate, id1);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//暗号化

	public static String password(String password) {

		String source = password;
		Charset charset = StandardCharsets.UTF_8;
		String algorithm = "MD5";
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック

		}
		String result = DatatypeConverter.printHexBinary(bytes);

		return result;
	}
}
